# dotfiles-public



## Getting started

Salin folder `.config/nvim` ke folder configmu misal username linux `agus` maka salin ke folder `/home/agus/.config`

Jalankan command ini diterminal kamu untuk menginstall paket manager nvim

```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

## Kebutuhan

* neovim versi 0.7.0 ke atas
* git 

Install aplikasi lsp server, misal kamu butuh autocomplete untuk typescript maka perlu menginstall language server seperti ini 

```
npm install -g typescript typescript-language-server
```

Untuk language server lain bisa baca dokumentasi yang lebih lengkap di alamat [LSP Server Configuration](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md)

## LSP config

Jika kamu tidak butuh lsp untuk bahasa pemrograman Rust, Go, Ruby, dll kamu bisa comment config pada file `.config/nvim/plugin/lspconfig.lua`. misalnya seperti ini 

```lua
-- rust analyzer
nvim_lsp.rust_analyzer.setup {
  on_attach = on_attach,
  filetypes = { "rust" },
  cmd = { "rust-analyzer" },
  capabilities = capabilities,
  flags = lsp_flags,
  settings = {
    ["rust-analyzer"] = {}
  }
}
```

Komen mulai dari `.setup {` sampai tutup kurung kurawalnya `}`

## Install

Tinggal buka neovim dan jalankan command `:PackerInstall` pada mode normal

## Ubah tema

bisa pake script seperti ini pada `init.lua`

```lua
vim.cmd([[
  syntax enable
  colorscheme onedark
]])
```
