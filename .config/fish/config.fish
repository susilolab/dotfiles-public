set fish_greeting

alias grep rg
alias cat bat
alias less bat
alias ls exa
alias ll exa
alias l exa
alias du dust
alias top ytop
#alias npm pnpm

set -gx SDA2_PATH "/mnt/sda2"
set -gx VOLTA_HOME "$HOME/.volta"
set -gx QT_QPA_PLATFORM wayland
set -gx GEM_HOME $HOME/var/Others/Ruby/gems
set -gx GEM_SPEC_CACHE $GEM_HOME/specs
set -gx ANDROID_HOME "$SDA2_PATH/susilo/var/Android"
set -gx RUSTC_WRAPPER sccache
set -gx SCCACHE_DIR /mnt/sda2/susilo/.cache/sccache
set -gx JDTLS_PATH "/home/susilo/var/Apps/jdt-language-server-1.24.0-202306011728"
set -gx KTS_PATH "/mnt/sda2/susilo/kts"
set -gx JAVA_HOME "/usr/lib/jvm/java-17-openjdk"
set -gx PATH "$VOLTA_HOME/bin" "$GEM_HOME/bin" "$HOME/.rvm/bin" "$ANDROID_HOME/flutter/bin" "$JDTLS_PATH/bin" "$KTS_PATH/bin" $PATH
set -g theme_color_scheme dracula

# Bun
set -Ux BUN_INSTALL "/home/susilo/.bun"
set -px --path PATH "/home/susilo/.bun/bin"

# pnpm
set -gx PNPM_HOME "/home/susilo/.local/share/pnpm"
set -gx PATH "$PNPM_HOME" $PATH
# pnpm end

set -gx WASMTIME_HOME "$HOME/.wasmtime"

string match -r ".wasmtime" "$PATH" > /dev/null; or set -gx PATH "$WASMTIME_HOME/bin" $PATH
