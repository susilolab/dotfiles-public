local status, toggleterm = pcall(require, "toggleterm")
if (not status) then return end

local options = {
  size = 10,
  open_mapping = [[<c-\>]],
  shading_factor = 2,
  direction = "float",
  float_opts = {
    border = "curved",
    highlights = {
      border = "Normal",
      background = "Normal",
    },
  },
}

toggleterm.setup(options)
