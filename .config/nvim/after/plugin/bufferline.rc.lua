local status, bufferline = pcall(require, "bufferline")
if (not status) then return end

bufferline.setup({
  options = {
    mode = "tabs",
    numbers = "ordinal",
    separator_style = 'slant', -- slant, thin, thick
    always_show_bufferline = false,
    show_buffer_close_icons = true,
    show_close_icon = true,
    color_icons = true,
    close_icon = '',
  }
  -- highlights = {
  -- separator = {
  -- guifg = '#073642',
  -- guibg = '#002b36',
  -- },
  -- separator_selected = {
  -- guifg = '#073642',
  -- },
  -- background = {
  -- guifg = '#657b83',
  -- guibg = '#002b36'
  -- },
  -- buffer_selected = {
  -- guifg = '#fdf6e3',
  -- gui = "bold",
  -- },
  -- fill = {
  -- guibg = '#073642'
  -- }
  -- },
})

-- Shift + Tab untuk pindah tab ke kanan
vim.keymap.set('n', '<Tab>', '<Cmd>BufferLineCycleNext<CR>', {})
-- Shift + Tab untuk pindah tab ke kiri
vim.keymap.set('n', '<S-Tab>', '<Cmd>BufferLineCyclePrev<CR>', {})
