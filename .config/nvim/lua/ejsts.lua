local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
parser_config.crystal = {
	install_info = {
		url = "https://github.com/tree-sitter/tree-sitter-embedded-template", -- local path or git repo
    requires_generate_from_grammar = true,
		files = {"src/parser.c"},
    branch = "main",
	},
  filetype = "ejs",
}
