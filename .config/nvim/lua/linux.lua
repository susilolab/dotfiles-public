local opt = vim.opt

-- Permanen undo
local HOME = os.getenv("HOME")
opt.undodir = HOME .. '/.vimdid'
opt.shell = '/usr/bin/fish'
