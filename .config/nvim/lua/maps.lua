local keymap = vim.keymap
local map = function(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end

local nmap = function(shortcut, command)
  map('n', shortcut, command)
end

nmap('<C-n>', ':NERDTreeToggle<CR>')
-- nmap('<leader>v', '<cmd>CHADopen<cr>')

-- Select all
keymap.set('n', '<C-a>', 'gg<S-v>G')

-- tutup semua tab dikanan
-- command -nargs=0 Tabr :.+1,$tabdo :tabc
vim.api.nvim_create_user_command('Tabr', ':.+1,$tabdo :tabc', { nargs = 0 })
