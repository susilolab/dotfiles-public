local opt = vim.opt

-- Permanen undo
local HOME = os.getenv("LOCALAPPDATA")
opt.undodir = HOME .. '/.vimdid'
