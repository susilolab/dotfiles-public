vim.cmd('autocmd!')
local opt = vim.opt

vim.scriptencoding = 'utf-8'
opt.encoding = 'utf-8'
opt.fileencoding = 'utf-8'
opt.syntax = 'on'

-- seting tab
opt.expandtab = true
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.smarttab = true

opt.autoindent = true
opt.cindent = true

-- njajal apa gunanya
opt.breakindent = true
opt.ai = true -- auto indent
opt.si = true -- smart indent
opt.wrap = false -- set nowrap
opt.path:append { '**' } -- search file sampai ke subfolder
opt.wildignore:append { '*/node_modules/*' }

vim.wo.number = true
opt.title = true
opt.autoindent = true
opt.hlsearch = true
opt.backup = false
opt.showcmd = true
opt.cmdheight = 1
opt.laststatus = 2
opt.mouse = 'a'
opt.scrolloff = 10
opt.number = true
opt.relativenumber = true

opt.splitright = true
opt.splitbelow = true
opt.hidden = true
opt.updatetime = 300
opt.shortmess = opt.shortmess + 'c'
opt.incsearch = true -- membuat pencarian seperti browser modern
opt.showmatch = true
opt.showmode = false

opt.undofile = true
vim.g.mapleader = ' '

-- setting clipboard
opt.clipboard = opt.clipboard + 'unnamedplus'
-- matikan semua bell
opt.belloff = 'all'
opt.termguicolors = true
opt.timeoutlen = 1000
opt.fileformats = opt.fileformats + 'unix'

vim.cmd([[
autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype elixir setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype rust setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype php setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype nginx setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype dockerfile setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype javascript setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype typescript setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType typescriptreact setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType javascriptreact setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype vue setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype json setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype html setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype xhtml setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype css setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype ruby setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype crystal setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype erlang setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType c setlocal shiftwidth=4 softtabstop=4 expandtab tabstop=4
autocmd FileType cpp setlocal shiftwidth=4 softtabstop=4 expandtab tabstop=4
autocmd FileType go setlocal shiftwidth=4 softtabstop=4 expandtab tabstop=4
autocmd FileType vala setlocal shiftwidth=4 softtabstop=4 expandtab tabstop=4
autocmd Filetype svelte setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype lua setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype twig setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype yaml setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype dart setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
]])
