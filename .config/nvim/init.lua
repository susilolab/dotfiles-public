require('base')
require('highlights')
require('maps')
require('plugins')
require('crystal-nvim')
require('ejsts')

local has = function(x)
  return vim.fn.has(x) == 1
end
local is_mac = has "macunix"
local is_win = has "win32" or has "win64"

if is_win then
  require('windows')
end

if not is_win and not is_mac then
  require('linux')
end

vim.cmd([[
  syntax enable
  colorscheme onedark
  autocmd BufNewFile,BufReadPost *.cr setlocal filetype=crystal
]])
