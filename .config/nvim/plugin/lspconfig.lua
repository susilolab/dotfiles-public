--vim.lsp.set_log_level("debug")

local status, nvim_lsp = pcall(require, "lspconfig")
if not status then
	return
end

local protocol = require("vim.lsp.protocol")

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
	local function buf_set_keymap(...)
		vim.api.nvim_buf_set_keymap(bufnr, ...)
	end

	local function buf_set_option(...)
		vim.api.nvim_buf_set_option(bufnr, ...)
	end

	--Enable completion triggered by <c-x><c-o>
	buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")

	-- Mappings.
	local opts = { noremap = true, silent = true }

	-- See `:help vim.lsp.*` for documentation on any of the below functions
	buf_set_keymap("n", "gD", "<Cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	buf_set_keymap("n", "gd", "<Cmd>lua vim.lsp.buf.definition()<CR>", opts)
	buf_set_keymap("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
	buf_set_keymap("n", "K", "<Cmd>lua vim.lsp.buf.hover()<CR>", opts)

	-- formatting
	-- disable formatting karena tidak cocok dengan format saya
	-- if client.server_capabilities.documentFormattingProvider then
	--     vim.api.nvim_create_autocmd("BufWritePre", {
	--         group = vim.api.nvim_create_augroup("Format", { clear = true }),
	--         buffer = bufnr,
	--         callback = function() vim.lsp.buf.format() end
	--     })
	-- end
end

protocol.CompletionItemKind = {
	"", -- Text
	"", -- Method
	"", -- Function
	"", -- Constructor
	"", -- Field
	"", -- Variable
	"", -- Class
	"ﰮ", -- Interface
	"", -- Module
	"", -- Property
	"", -- Unit
	"", -- Value
	"", -- Enum
	"", -- Keyword
	"", -- Snippet
	"", -- Color
	"", -- File
	"", -- Reference
	"", -- Folder
	"", -- EnumMember
	"", -- Constant
	"", -- Struct
	"", -- Event
	"ﬦ", -- Operator
	"", -- TypeParameter
}

-- Set up completion using nvim_cmp with LSP source
local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

local lsp_flags = {
	debounce_text_changes = 150,
}

nvim_lsp.flow.setup({
	on_attach = on_attach,
	capabilities = capabilities,
})

nvim_lsp.tsserver.setup({
	on_attach = on_attach,
	filetypes = { "typescript", "typescriptreact", "typescript.tsx", "javascript" },
	cmd = { "typescript-language-server", "--stdio" },
	capabilities = capabilities,
})

-- rust analyzer
nvim_lsp.rust_analyzer.setup({
	capabilities = capabilities,
	on_attach = on_attach,
	-- filetypes = { "rust" },
	cmd = { "rustup", "run", "stable", "rust-analyzer" },
	-- cmd = { "rust-analyzer" },
	-- flags = lsp_flags,
	settings = {
		["rust-analyzer"] = {},
	},
})

-- jdtls
-- nvim_lsp.jdtls.setup {
--     on_attach = on_attach,
--     filetypes = { "java" },
--     cmd = { "jdtls", "-configuration", "/home/susilo/.cache/jdtls/config", "-data", "/home/susilo/.cache/jdtls/workspace" },
--     capabilities = capabilities,
--     flags = lsp_flags,
-- }

-- golang
nvim_lsp.gopls.setup({
	on_attach = on_attach,
	filetypes = { "go", "gomod", "gowork", "gotmpl" },
	cmd = { "gopls" },
	capabilities = capabilities,
	flags = lsp_flags,
	settings = {
		analyses = {
			unusedparams = true,
			shadow = true,
		},
		staticcheck = true,
	},
})

nvim_lsp.kotlin_language_server.setup({
	single_file_support = true,
})

-- clojure
nvim_lsp.clojure_lsp.setup({
	on_attach = on_attach,
	filetypes = { "clojure", "edn" },
	cmd = { "clojure-lsp" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- php
nvim_lsp.intelephense.setup({
	on_attach = on_attach,
	filetypes = { "php" },
	cmd = { "intelephense", "--stdio" },
	capabilities = capabilities,
	flags = lsp_flags,
})
require("flutter-tools").setup({})

-- elixirls
nvim_lsp.elixirls.setup({
	on_attach = on_attach,
	cmd = { "/home/susilo/var/Apps/elixir_ls/language_server.sh" },
	filetypes = { "elixir", "eelixir" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- crystal
nvim_lsp.crystalline.setup({
	on_attach = on_attach,
	cmd = { "crystalline" },
	filetypes = { "crystal" },
	capabilities = capabilities,
	flags = lsp_flags,
	single_file_support = true,
})

-- denols
nvim_lsp.denols.setup({
	on_attach = on_attach,
	cmd = { "deno", "lsp" },
	filetypes = { "javascript", "javascriptreact", "javascript.jsx", "typescript", "typescriptreact", "typescript.tsx" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- dartls
nvim_lsp.dartls.setup({
	on_attach = on_attach,
	cmd = { "dart", "language-server", "--protocol=lsp" },
	filetypes = { "dart" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- erlangls
nvim_lsp.erlangls.setup({
	on_attach = on_attach,
	cmd = { "/usr/local/bin/erlang_ls" },
	filetypes = { "erlang" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- python
nvim_lsp.pyright.setup({
	on_attach = on_attach,
	cmd = { "pyright-langserver", "--stdio" },
	filetypes = { "python" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- ruby
nvim_lsp.solargraph.setup({
	on_attach = on_attach,
	cmd = { "solargraph", "stdio" },
	filetypes = { "ruby" },
	capabilities = capabilities,
	flags = lsp_flags,
	init_options = {
		formatting = true,
	},
})

-- ccls
-- nvim_lsp.ccls.setup {
--   on_attach = on_attach,
--   cmd = { "ccls" },
--   filetypes = { "c", "cpp", "objc", "objcpp" },
--   capabilities = capabilities,
--   flags = lsp_flags,
--   init_options = {
--     cache = {
--       directory = ".ccls_cache"
--     }
--   }
-- }

-- clangd
nvim_lsp.clangd.setup({
	on_attach = on_attach,
	cmd = { "clangd" },
	filetypes = { "c", "cpp", "objc", "objcpp" },
	capabilities = capabilities,
	flags = lsp_flags,
	init_options = {
		formatting = false,
	},
})

-- zls
nvim_lsp.zls.setup({
	on_attach = on_attach,
	cmd = { "zls" },
	filetypes = { "zig", "zir" },
	capabilities = capabilities,
	flags = lsp_flags,
})

-- vala_ls
nvim_lsp.vala_ls.setup({
	on_attach = on_attach,
	cmd = { "vala-language-server" },
	filetypes = { "vala", "genie" },
	capabilities = capabilities,
	flags = lsp_flags,
})

nvim_lsp.sourcekit.setup({
	on_attach = on_attach,
})

nvim_lsp.lua_ls.setup({
	on_attach = on_attach,
	settings = {
		Lua = {
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { "vim" },
			},

			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
				checkThirdParty = false,
			},
			telemetry = {
				enable = false,
			},
		},
	},
})

nvim_lsp.tailwindcss.setup({})
-- handler
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
	underline = true,
	update_in_insert = false,
	virtual_text = { spacing = 4, prefix = "●" },
	severity_sort = true,
})

-- Diagnostic symbols in the sign column (gutter)
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

vim.diagnostic.config({
	virtual_text = {
		prefix = "●",
	},
	update_in_insert = true,
	float = {
		source = "always", -- Or "if_many"
	},
})
